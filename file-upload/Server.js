var express	=	require("express");
var multer	=	require('multer');
var app	=	express();
var storage	=	multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, '/uploads');
  },
  filename: function (req, file, callback) {
    callback(null, file.fieldname + '-' + Date.now());
  }
});
var upload = multer({ storage : storage}).single('userPhoto');
const axios = require('axios');

// TODO: Show Wallet Address In UI
//       would be cool to accept payment from users in case the
//       the server runs out of LBC tokens

app.get('/',function(req,res){
      res.sendFile(__dirname + "/index.html");
      axios.post('http://localhost:5279',
               {
                   method: 'address_unused',
                   params: {},
               })
        .then(function (resp) {console.log("LBC wallet address: " + resp.data.result);})
        .catch(function (err) {console.log('You should start lbrynet, and setup a wallet');})
});

// HACK: channel name is hardcoded, fuuu..
// TODO: asynchronously set up a wallet using API, get address, create channel
//       if you're gonna seriously use this, probably should add these ^
const path = require('path');

app.post('/api/photo',function(req,res){
    upload(req,res,function(err) {
        axios.post("http://localhost:5279",
        {"method": "stream_create",
         "params": {"name": req.file.filename,
                    "bid": "0.01",
                    "file_path": path.resolve("/uploads", req.file.filename),
                    "validate_file": false,
                    "channel_name": "@rage-reports",
                    "optimize_file": false,
                    "tags": [],
                    "preview": false,
                    "blocking": false}})
            .then(function(res){ console.log('posted to https://odysee.com/@rage-reports')})
            .catch(function(err){ console.log('oopsie, cant post to LBRY')})

		if(err) {
			return res.end("Error uploading file.");
		}
		res.end("File is uploaded");
  });
});

app.listen(3000,function(){
    console.log("Working on port 3000");
});
