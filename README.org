Deploying/Running...

* Upload middleman
  This is a nodejs server that downloads files from clients and queues
  them to be uploaded to LBRY.

  run it with:
  - cd file-upload
  - `cat default.nix`
  - npm install
  - node Server.js

* Lbrynet HTTP/JSON API server
  This is the official sdk to interact with the LBRY network, can do
  things like publish, and download files. Needs wallet and LBC tokens
  to upload.

  - cd lbry-sdk
  - `cat ../lbry-sdk.nix`
  - python -m venv .venv
  - source .venv/bin/activate
  - make
  - lbrynet start
